# README #

## Modo de uso ##
Para crear el archivo jar tienes que escribir en consola "make practica" tras esto, tendras que escribir en consola
"java -jar practica.jar nº_lanzamiento" donde nº_lanzamiento es el número de dardos que quieres lanzar, cuanto más
dardos se lance más preciso será el número pi.

## Diagrama UML ##
![alt text](https://bitbucket.org/programacion1tr/practica22/raw/2b447721467e029038bb01cdc452a38f66a5e8a6/diagrama%20de%20clases.png)

## Funcionamiento ##
El programa funciona haciendo el método de Montecarlos basado en que para aproximar el valor de Pi consiste en dibujar 
un cuadrado, y dentro de ese cuadrado, dibujar un círculo con diámetro de igual medida que uno de los lados del cuadrado. 
Luego se lanzan dardos de manera aleatoria sobre la superficie dibujada. Dependiendo del número de dardos que hayan caido
dentro y fuera sale un número u otro tras realizar ciertas operaciones. El programa básicamente se encarga de lanzar esos
dardos de manera recursiva y de hacer la operación.