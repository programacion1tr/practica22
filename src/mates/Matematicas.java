/*Copyright 2022 Pablo Trujillo
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the
License.*/

package mates;
import java.util.*;

public class Matematicas{
	
	//Ponemos los contadores de dentro y fuera fuera del código para que no se reinice todo el rato	
	static double dentro = 0;
	static double fuera = 0;
	public static double generarNumeroPiRecursivo(long pasos){
		//Crea un número al azar y hace una operación para saber si cae dentro o fuera y lo suma a el contador donde ha caido
		Random rand = new Random();
                double x = 2*(rand.nextDouble())-1;
                double y = 2*(rand.nextDouble())-1;
                double w = Math.sqrt(x*x+y*y);
		if (w<=1){
			dentro ++;
		}
		else{
			fuera ++;
		}
		//Hace que si es uno escriba que ha terminado los calculos, y que si no repita la operación restándole uno a los pasos
		if (pasos == 1){
			System.out.println("Calculos terminados.");
		}
		else{
			generarNumeroPiRecursivo(pasos-1);
		}
		//Calcula pi dependiendo de cuantos dardos hayan caido dentro y fuera
		double q = ((4*dentro)/(dentro + fuera));
		return q;
	}
}
